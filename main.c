#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>
#include"src/liste_figure.h"
#include"src/figures/point.h"
#include"src/gestion_evenement.h"

int inialisation_SDL(SDL_Window ** fenetre, int largeur, int hauteur, SDL_Renderer ** renderer);
void fermeture_SDL(SDL_Window * fenetre, SDL_Renderer * renderer);
void gestion_evenements(int * close);
void rendu_fenetre(SDL_Renderer * renderer, liste_figure_t * l);



int main(int argc, char *argv[])
{
  SDL_Window * fenetre;
  SDL_Renderer * rendu;
  int close = inialisation_SDL(&fenetre, 1000, 1000, &rendu);
  if( close < 0 )
    return EXIT_FAILURE;

  // Cette partie sert d'exemple de création de figures et NE DOIT PLUS ETRE dans le rendu final.
  liste_figure_t * l = creer_liste();
  for( int i = 0; i < 1000; i++)
    liste_inserer_fin(l, creer_point(i, 500, creer_couleur(255,0,0)));
  for( int i = 0; i < 1000; i++){
    liste_inserer_fin(l, creer_point(500, i, creer_couleur(0,0,255)));
  }
  // fin de l'exemple

  while (!close) {
    gestion_evenements(&close);
    rendu_fenetre(rendu, l);    
  }

  fermeture_SDL(fenetre, rendu);
  return EXIT_SUCCESS;
}



void rendu_fenetre(SDL_Renderer * rendu, liste_figure_t * l){
  SDL_SetRenderDrawColor(rendu, 255, 255, 255, 255);
  SDL_RenderClear(rendu);

  for(maillon_figure_t * tmp = l->premier; tmp != NULL; tmp = tmp->suivant){
    tmp->figure->afficher(rendu, tmp->figure);
  }
 
  SDL_RenderPresent(rendu);  
}



int inialisation_SDL(SDL_Window ** fenetre, int largeur, int hauteur, SDL_Renderer ** rendu){
  // Quitte le programme en cas d'erreur
  if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
    printf("error initializing SDL: %s\n", SDL_GetError());
    return -1;
  }
  *fenetre = SDL_CreateWindow("PROJET L2 Structures Algébriques",
			      SDL_WINDOWPOS_CENTERED,
			      SDL_WINDOWPOS_CENTERED,
			      largeur, hauteur, 0);
  *rendu = SDL_CreateRenderer(*fenetre, -1, 0);
  return 0;
}



void fermeture_SDL(SDL_Window * fenetre, SDL_Renderer * rendu){
  SDL_DestroyRenderer(rendu);
  SDL_DestroyWindow(fenetre);   
  SDL_Quit();
}




